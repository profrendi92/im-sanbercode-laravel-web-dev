<?php

require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo "Nama domabanya : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 4
echo "Hewan Berdarah Dingin : $sheep->cold_blooded <br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");

echo "Nama kodoknya : $kodok->name <br>";
echo "Jumlah Kaki : $kodok->legs <br>";
echo "Hewan Berdarah Dingin : $kodok->cold_blooded <br>";
$kodok->jump();


$sungokong = new Ape("kera sakti");

echo "<br><br> Nama monyetnya : $sungokong->name <br>";
echo "Jumlah Kaki : $sungokong->legs <br>";
echo "Hewan Berdarah Dingin : $sungokong->cold_blooded <br>";
$sungokong->yell();



?>