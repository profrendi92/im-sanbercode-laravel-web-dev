<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('register');
    }
    public function sambutan(Request $request){
        $firstName = $request->input('fname');
        $lastName = $request->input('lname');
        $negara = $request->input('nationality');
        
        return view('welcome',["namaDepan" => $firstName, "namaBelakang" => $lastName, "bangsa" => $negara]);
    }

}
