<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'daftar']);
Route::post('/welcome', [AuthController::class, 'sambutan']);

Route::get('/table', function(){
    return view('partial.table');
});

Route::get('/data-table', function(){
    return view('partial.data-table');
});

// C => Create Data (membuat inputan form ke table cast)
Route::get('/cast/create', [CastController::class, 'create']);
// Insert Inputan ke Database Table cast
Route::post('/cast', [CastController::class, 'store']);

// R => Read Data (menampilkan semua pada table cast)
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// U => Update Data (mengarah pada form edit data dengan params id)
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// D => Delete Data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
