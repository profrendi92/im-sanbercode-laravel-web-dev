@extends('layout.master')

@section ('judul')
Detail Biodata Pemain
@endsection

@section ('content')

<h2>{{$cast->nama}}</h2>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection