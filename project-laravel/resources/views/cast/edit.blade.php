@extends('layout.master')

@section ('judul')
Halaman Update Pemain
@endsection

@section ('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" @error('nama') is-invalid @enderror">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="text"  name="umur" value="{{$cast->umur}}" class="form-control" @error('umur') is-invalid @enderror">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Masukkan Biodata</label>
        <textarea name="bio" class="form-control" @error('bio') is-invalid @enderror" cols="30" rows="10">{{$cast->bio}}</textarea>
      </div>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-info">Cancel</a>
  </form>

@endsection