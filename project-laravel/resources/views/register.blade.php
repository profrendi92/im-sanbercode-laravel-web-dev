<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM SIGN UP</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <H3>Sign Up Form</H3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Rusian</option>
            <option value="3">United States</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language"> Bahasa Indonesia <br>
        <input type="checkbox" name="language"> English <br>
        <input type="checkbox" name="language"> Other <br><br>
        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br><br>
        
            
   <input type="submit" value="Kirim">
   </form>
</body>
</html>