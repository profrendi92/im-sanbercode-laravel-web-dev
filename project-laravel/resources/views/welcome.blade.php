<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SELAMAT DATANG!</title>
</head>
<body>
    <h1>SELAMAT DATANG! {{$namaDepan}} {{$namaBelakang}}</h1>
    <h2>
        Kewarganegaraan anda adalah
            @if($bangsa === "1")
                Indonesia
            @elseif($bangsa === "2")
                Russian
            @else
                United States
            @endif
    </h2>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
</body>
</html>